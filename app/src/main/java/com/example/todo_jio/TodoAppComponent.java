package com.example.todo_jio;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;

@Singleton
@Component(modules = TodoAppModule.class)
interface TodoAppComponent extends AndroidInjector<TodoApp> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<TodoApp> {

    }
}
