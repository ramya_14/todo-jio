package com.example.todo_jio.codeviews;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.todo_jio.models.Todo;
import com.example.todo_jio.presenters.ListContract;
import com.example.todo_jio.presenters.ListPresenterImpl;
import com.example.todo_jio.R;
import com.example.todo_jio.common.BaseViewFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListFragment extends BaseViewFragment<ListContract.ListTodoPresenter> implements ListContract.ListTodoView {

    private ArrayList<Todo> taskList = new ArrayList<>();
    private Todo todo;

    @BindView(R.id.rvList)
    RecyclerView rvList;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    private TodoAdapter adapter;
    private ListContract.ListTodoPresenter presenter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Notes");

        adapter = new TodoAdapter(getActivity(), taskList);
        rvList.setAdapter(adapter);
        rvList.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        presenter = new ListPresenterImpl();
        updateUI();
        return view;
    }

    @OnClick(R.id.fab)
    public void onClick(View view) {

        addFragment(R.id.container, new AddFragment());
    }

    private void updateUI() {
        taskList.clear();
        taskList.addAll(presenter.fetchFromDB(getActivity()));
        adapter.notifyDataSetChanged();
    }
}
