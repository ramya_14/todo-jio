package com.example.todo_jio.presenters;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.todo_jio.models.Todo;
import com.example.todo_jio.models.TodoDBHelper;
import com.example.todo_jio.common.presenter.BasePresenter;
import com.example.todo_jio.inject.PerFragment;

import javax.inject.Inject;

@PerFragment
public class AddPresenterImpl extends BasePresenter<AddContract.AddTodoView> implements AddContract.AddTodoPresenter {

    @Inject
    public AddPresenterImpl() {
        super();
    }

    @Override
    public void updateTodoDB(Context context, Todo todo) {
        TodoDBHelper helper = new TodoDBHelper(context);
        helper.addToDatabase(todo);
    }
}
