package com.example.todo_jio;

import android.app.Application;
import android.app.ListFragment;

import com.example.todo_jio.codeviews.ListFragmentModule;
import com.example.todo_jio.codeviews.MainActivity;
import com.example.todo_jio.codeviews.MainActivityModule;
import com.example.todo_jio.inject.PerActivity;
import com.example.todo_jio.inject.PerFragment;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.android.AndroidInjection;
import dagger.android.AndroidInjectionModule;
import dagger.android.ContributesAndroidInjector;

@Module(includes = AndroidInjectionModule.class)
abstract class TodoAppModule {


    @Binds
    @Singleton
    abstract Application application(TodoApp app);

    @PerActivity
    @ContributesAndroidInjector(modules = MainActivityModule.class)
    abstract MainActivity mainActivityInjector();

}
