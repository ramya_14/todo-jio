package com.example.todo_jio.presenters;

import android.content.Context;

import com.example.todo_jio.models.Todo;
import com.example.todo_jio.common.presenter.Presenter;

public interface AddContract extends Presenter{

    interface AddTodoView{
    }

    interface AddTodoPresenter extends Presenter {
        void updateTodoDB(Context context, Todo todo);
    }
}
