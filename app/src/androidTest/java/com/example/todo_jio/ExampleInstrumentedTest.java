package com.example.todo_jio;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.example.todo_jio.models.Todo;
import com.example.todo_jio.models.TodoDBHelper;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static android.support.test.InstrumentationRegistry.getTargetContext;
import static android.support.test.InstrumentationRegistry.registerInstance;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    private TodoDBHelper database;


    @Before
    public void setUp() throws Exception {
        database = new TodoDBHelper(getTargetContext());
        database.deleteTable();
    }

    @After
    public void tearDown() throws Exception {
        database.close();
    }

    @Test
    public void shouldAddExpenseType() throws Exception {
        database.addToDatabase(new Todo("TestTitle", "TestNote"));

        List<String> tasks = database.getValues(TodoDBHelper.TABLE);
        assertThat(tasks.size(), is(1));
        assertTrue(tasks.get(0).equals("TestTitle"));
    }


    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = getTargetContext();

        assertEquals("com.example.todo_jio", appContext.getPackageName());
    }
}
