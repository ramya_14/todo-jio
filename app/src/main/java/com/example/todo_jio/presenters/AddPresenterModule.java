package com.example.todo_jio.presenters;

import com.example.todo_jio.inject.PerFragment;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class AddPresenterModule {

    @Binds
    @PerFragment
    abstract AddContract.AddTodoPresenter
    addTodoPresenter(AddPresenterImpl addPresenterImpl);
}
