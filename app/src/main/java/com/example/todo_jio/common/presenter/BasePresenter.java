package com.example.todo_jio.common.presenter;


import android.os.Bundle;
import android.support.annotation.Nullable;

public abstract class BasePresenter<T> implements Presenter {

    protected BasePresenter() {
    }

    @Override
    public void onStart(@Nullable Bundle savedInstanceState) {
    }

    @Override
    public void onResume() {
    }

    @Override
    public void onPause() {
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }

    @Override
    public void onEnd() {
    }
}
