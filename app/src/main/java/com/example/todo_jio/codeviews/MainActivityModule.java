package com.example.todo_jio.codeviews;


import android.app.Activity;


import com.example.todo_jio.common.base.BaseActivityModule;
import com.example.todo_jio.inject.PerActivity;
import com.example.todo_jio.inject.PerFragment;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module(includes = BaseActivityModule.class)
public abstract class MainActivityModule {

    @PerFragment
    @ContributesAndroidInjector(modules = ListFragmentModule.class)
    abstract ListFragment ListFragmentInjector();

    @PerFragment
    @ContributesAndroidInjector(modules = AddFragmentModule.class)
    abstract AddFragment addFragmentInjector();

    @Binds
    @PerActivity
    abstract Activity activity(MainActivity mainActivity);
}
