package com.example.todo_jio.models;

public class Todo {

    private String mId;
    private String mTitle;
    private String mNote;

    public Todo() {
    }

    public Todo(String title, String note) {
        mTitle = title;
        mNote = note;
    }

    public Todo(String id, String title, String note) {
        mId = id;
        mTitle = title;
        mNote = note;
    }

    public String getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getNote() {
        return mNote;
    }

    public void setId(String id) {
        mId = id;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setNote(String note) {
        mNote = note;
    }
}

