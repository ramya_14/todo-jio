package com.example.todo_jio.codeviews;

import android.app.*;

import com.example.todo_jio.presenters.ListContract;
import com.example.todo_jio.presenters.ListPresenterModule;
import com.example.todo_jio.common.base.BaseFragmentModule;
import com.example.todo_jio.inject.PerFragment;

import javax.inject.Named;

import dagger.Binds;
import dagger.Module;

@Module(includes = {
        BaseFragmentModule.class,
        ListPresenterModule.class
})
public abstract class ListFragmentModule {

    @Binds
    @Named(BaseFragmentModule.FRAGMENT)
    @PerFragment
    abstract Fragment fragment(ListFragment listFragment);

    @Binds
    @PerFragment
    abstract ListContract.ListTodoView listTodoView(ListFragment listFragment);

}
