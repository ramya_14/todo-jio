package com.example.todo_jio.codeviews;

import android.app.Fragment;

import com.example.todo_jio.presenters.AddContract;
import com.example.todo_jio.presenters.AddPresenterModule;
import com.example.todo_jio.common.base.BaseFragmentModule;
import com.example.todo_jio.inject.PerFragment;

import javax.inject.Named;

import dagger.Binds;
import dagger.Module;

@Module(includes = {
        BaseFragmentModule.class,
        AddPresenterModule.class
})
public abstract class AddFragmentModule {

    @Binds
    @Named(BaseFragmentModule.FRAGMENT)
    @PerFragment
    abstract Fragment fragment(AddFragment addFragment);

    @Binds
    @PerFragment
    abstract AddContract.AddTodoView addTodoView(AddFragment addTodoFragment);
}
