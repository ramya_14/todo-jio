package com.example.todo_jio.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class TodoDBHelper extends SQLiteOpenHelper {

    public static final String DB = "todo_db";
    public static final String TABLE = "tasks";
    public static final String COL_TASK_ID = "_id";
    public static final String COL_TASK_TITLE = "title";
    public static final String COL_TASK_NOTE = "note";


    public TodoDBHelper(Context context) {
        super(context, DB, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " +
                TABLE +
                "( " + COL_TASK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COL_TASK_TITLE + " TEXT NOT NULL, " +
                COL_TASK_NOTE + " TEXT NOT NULL);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // DROP table
        db.execSQL("DROP TABLE IF EXISTS todo");
        // Recreate table
        onCreate(db);
    }

    public void deleteTable() {
        SQLiteDatabase db = getReadableDatabase();
        db.delete(TABLE, null, null);

    }

    public void addToDatabase(Todo todo){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TodoDBHelper.COL_TASK_TITLE, todo.getTitle());
        values.put(TodoDBHelper.COL_TASK_NOTE, todo.getNote());
        db.insertWithOnConflict(TodoDBHelper.TABLE,
                null,
                values,
                SQLiteDatabase.CONFLICT_REPLACE);
        db.close();
    }

    public ArrayList<String> getValues(String table) {
        ArrayList<String> values = new ArrayList<String>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT title FROM " + table, null);

        if(cursor.moveToFirst()) {
            do {
                values.add(cursor.getString(cursor.getColumnIndex("title")));
            }while(cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return values;
    }


}
