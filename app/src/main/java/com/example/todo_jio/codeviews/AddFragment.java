package com.example.todo_jio.codeviews;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.todo_jio.models.Todo;
import com.example.todo_jio.presenters.AddContract;
import com.example.todo_jio.presenters.AddPresenterImpl;
import com.example.todo_jio.R;
import com.example.todo_jio.common.BaseViewFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddFragment extends BaseViewFragment<AddContract.AddTodoPresenter> implements AddContract.AddTodoView {

    @BindView(R.id.edtTitle)
    EditText edtTitle;

    @BindView(R.id.edtNote)
    EditText edtNote;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    private AddContract.AddTodoPresenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add, container, false);
        ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Add Note");
        presenter = new AddPresenterImpl();
        return view;
    }

    @OnClick(R.id.btnSubmit)
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSubmit: {
                onAddTodo();
                break;
            }
        }
    }

    private void onAddTodo() {
        if (!TextUtils.isEmpty(edtTitle.getText().toString()) && !TextUtils.isEmpty(edtNote.getText().toString())) {
            Todo todo = new Todo();
            todo.setTitle(edtTitle.getText().toString());
            todo.setNote(edtNote.getText().toString());
            presenter.updateTodoDB(getActivity(),todo);
            getFragmentManager().popBackStack();

        } else {
            Toast.makeText(getActivity(), "Please enter title & description", Toast.LENGTH_SHORT).show();
        }
    }

}
