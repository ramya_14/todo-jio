package com.example.todo_jio.presenters;

import android.content.Context;

import com.example.todo_jio.models.Todo;
import com.example.todo_jio.common.presenter.Presenter;

import java.util.ArrayList;

public interface ListContract extends Presenter {
    interface ListTodoView {
        //void updateFetchResult(ArrayList<Todo> tasklist);
        // void updateList(Todo todo);
    }

    interface ListTodoPresenter extends Presenter {
        ArrayList<Todo> fetchFromDB(Context context);
    }
}
