package com.example.todo_jio.codeviews;

import android.os.Bundle;

import com.example.todo_jio.R;
import com.example.todo_jio.common.base.BaseActivity;


public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            addFragment(R.id.container,new ListFragment());
        }
    }
}
