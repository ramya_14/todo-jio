package com.example.todo_jio.presenters;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.todo_jio.models.Todo;
import com.example.todo_jio.models.TodoDBHelper;
import com.example.todo_jio.common.presenter.BasePresenter;

import java.util.ArrayList;

import javax.inject.Inject;

public class ListPresenterImpl extends BasePresenter<ListContract.ListTodoView> implements ListContract.ListTodoPresenter {

    @Inject
    public ListPresenterImpl() {
        super();
    }

    @Override
    public  ArrayList<Todo> fetchFromDB(Context context) {
        ArrayList<Todo> taskList = new ArrayList<>();
        Todo todo;
        TodoDBHelper helper = new TodoDBHelper(context);
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.query(TodoDBHelper.TABLE,
                new String[]{TodoDBHelper.COL_TASK_ID,
                        TodoDBHelper.COL_TASK_TITLE, TodoDBHelper.COL_TASK_NOTE},
                null, null, null, null, null);
        while (cursor.moveToNext()) {
            todo = new Todo();
            int title = cursor.getColumnIndex(TodoDBHelper.COL_TASK_TITLE);
            int note = cursor.getColumnIndex(TodoDBHelper.COL_TASK_NOTE);
            todo.setTitle(cursor.getString(title));
            todo.setNote(cursor.getString(note));
            taskList.add(todo);
        }
        cursor.close();
        db.close();
        return taskList;
    }
}
