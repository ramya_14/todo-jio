package com.example.todo_jio.codeviews;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.todo_jio.models.Todo;
import com.example.todo_jio.R;

import java.util.ArrayList;

public class TodoAdapter extends RecyclerView.Adapter<TodoAdapter.TodoHolder> {

    private ArrayList<Todo> items;
    private LayoutInflater inflater;

    public TodoAdapter(Context context, ArrayList<Todo> items) {
        this.items = items;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getItemCount() {
        Log.d("Main", "here" + items.size());
        return items.size();
    }

    @Override
    public void onBindViewHolder(TodoHolder holder, int position) {
        Todo todo = getItem(position);
        holder.txtTitle.setText(todo.getTitle());
        holder.txtNote.setText(todo.getNote());
    }

    @Override
    public TodoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.
                inflate(R.layout.todo_grid, parent, false);
        return new TodoHolder(view);
    }

    protected Todo getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class TodoHolder extends RecyclerView.ViewHolder {
        TextView txtTitle;
        TextView txtNote;

        public TodoHolder(View view) {
            super(view);
            txtTitle = view.findViewById(R.id.txtTitle);
            txtNote = view.findViewById(R.id.txtNote);
        }
    }
}
